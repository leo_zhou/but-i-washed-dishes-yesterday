function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

angular.module('todo', ['ionic', "firebase"])

.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider
    .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html',
      controller: 'LoginCtrl'
    })
    .state('main', {
      url: '/main',
      templateUrl: 'templates/main.html',
      controller: 'TodoCtrl'
    })
    .state('signup', {
      url: '/signup',
      templateUrl: 'templates/signup.html',
      controller: 'SignUpCtrl'
    });

  $urlRouterProvider.otherwise('/login');
})

/**
 * The Projects factory handles saving and loading projects
 * from local storage, and also lets us save and load the
 * last active project index.
 */
.factory('Projects', [ "$firebaseArray", "$firebaseObject",
  function($firebaseArray, $firebaseObject) {
  return {
    save: function(projects) {
      window.localStorage['projects'] = angular.toJson(projects);
    },
    newProject: function(projectTitle) {
      // Add a new project
      return {
        title: projectTitle,
        tasks: []
      };
    },
    getLastActiveIndex: function() {
      return parseInt(window.localStorage['lastActiveProject']) || 0;
    },
    setLastActiveIndex: function(index) {
      window.localStorage['lastActiveProject'] = index;
    },
    getTasks: function(project) {
      console.log(project)
      var householdsRef = new Firebase("https://biwdy.firebaseio.com/Households");
      return $firebaseArray(householdsRef.child(project.key + "/chores"))
    }
  }
}])

.controller('TodoCtrl', function($scope, $timeout, $ionicModal, Projects, $ionicSideMenuDelegate,
                                 $state, $firebaseObject, $firebaseArray, $ionicActionSheet) {
  var baseRef = new Firebase("https://biwdy.firebaseio.com/");
  $("#firstNewHouse").hide();
  $scope.baseRef = baseRef;

  // A utility function for creating a new project
  // with the given projectTitle
  var createProject = function(projectTitle) {
    var newProject = Projects.newProject(projectTitle);
    $scope.projects.push(newProject);
    Projects.save($scope.projects);
    $scope.selectProject(newProject, $scope.projects.length-1);
  }

  $scope.alert = window.alert;

  // get current user
  var authData = baseRef.getAuth();
  if (authData === null) {
    $state.go("login");
  } else {
    $scope.currentUser = authData.uid;
  }

  var currentUserRef = $scope.baseRef.child("Users").child($scope.currentUser);
  $firebaseObject(currentUserRef).$bindTo($scope, "currentUserInfo");

  // Load household list
  var currentHouseholdsRef = currentUserRef.child("households");
  var obj = $firebaseObject(currentHouseholdsRef)
  $scope.projects = []

  var selected_project = Projects.getLastActiveIndex();

  currentHouseholdsRef.on("child_added", function(snapshot) {
    console.log(snapshot.key(), snapshot.val());
    var householdRef = baseRef.child("Households").child(snapshot.key());
    if (snapshot.key() === selected_project || selected_project === 0) {
      if (selected_project === 0) selected_project === null;
      householdRef.once("value", function() {
        $("div.spinner").hide();
      })
      $firebaseObject(householdRef).$bindTo($scope, "activeProject");
    }
    $scope.projects.push( $firebaseObject(householdRef) );
  });

  currentHouseholdsRef.once("value", function(snapshot) {
    console.log(snapshot);
    if (snapshot.val() === null) {
      $("#firstNewHouse").show();
      $("div.spinner").hide();
    }
  });

  // Called to create a new project
  $scope.newProject = function() {
    var projectTitle = prompt('Project name');
    if(projectTitle) {
      createProject(projectTitle);
    }
  };

  // Called to select the given project
  $scope.selectProject = function(project, index) {
    $scope.activeProject = project;
    Projects.setLastActiveIndex(index);
    $ionicSideMenuDelegate.toggleLeft(false);
  };

  // Create our modal
  $ionicModal.fromTemplateUrl('new-task.html', function(modal) {
    $scope.taskModal = modal;
  }, {
    scope: $scope
  });

  $scope.createTask = function(task) {
    if ($scope.activeProject.chores === undefined) $scope.activeProject.chores = {};

    for (var k in $scope.activeProject.chores) {
      c = $scope.activeProject.chores[k];
      console.log(c.name, task.name);
      if (c.name === task.name) {
        alert("name duplicated");
        console.log($scope);
        task.name = "";
        return;
      }
    }

    var t = {
      name: task.name,
      counts: {}
    }
    t.counts[$scope.currentUser] = 0;

    $scope.taskModal.hide();
    task.name = "";
    $scope.activeProject.chores[guid()] = t;
  };

  $scope.newTask = function() {
    $scope.taskModal.show();
  };

  $scope.closeNewTask = function() {
    $scope.taskModal.hide();
  };

  // Create house modal
  $ionicModal.fromTemplateUrl('new-house.html', function(modal) {
    $scope.houseModal = modal;
  }, {
    scope: $scope
  });

  $scope.createHouse = function(house) {
    var householdKey = guid();
    var newHouseRef = baseRef.child("Households").child(householdKey)

    newHouseRef.child("name").set(house.name);
    newHouseRef.child("users").child($scope.currentUser).set("true");

    baseRef.child("Users").child($scope.currentUser).child("households").child(householdKey).set(true);

    $scope.houseModal.hide();
    $("#firstNewHouse").hide();
  };

  $scope.newHouse = function() {
    $scope.houseModal.show();
  };

  $scope.closeNewHouse = function() {
    $scope.houseModal.hide();
  };

  // Create house modal
  $ionicModal.fromTemplateUrl('add-friend.html', function(modal) {
    $scope.friendModal = modal;
  }, {
    scope: $scope
  });

  var usersRef = $scope.baseRef.child("Users");
  $scope.users = $firebaseObject(usersRef);

  $scope.addFriend = function(uid) {
    baseRef.child("Households").child($scope.activeProject.$id).child("users").child(uid).set(true);
    usersRef.child(uid).child("households").child($scope.activeProject.$id).set(true);
    $scope.friendModal.hide();
  };

  $scope.toggleProjects = function() {
    $ionicSideMenuDelegate.toggleLeft();
  };

  $scope.incCount = function(task_key) {
    var counts = $scope.activeProject.chores[task_key].counts
    if (typeof counts === 'undefined') counts = {};

    var currentUserCount = counts[$scope.currentUser];
    if(typeof currentUserCount === 'undefined') counts[$scope.currentUser] = 0;
    counts[$scope.currentUser] += 1;

    $scope.activeProject.chores[task_key].counts = counts;
  };

  $scope.logout = function() {
    var ref = new Firebase("https//biwdy.firebaseio.com/");
    ref.unauth();
    $state.go('login');
  }

  $scope.onHold = function(task_key) {
    $ionicActionSheet.show({
      buttons: [
       { text: '-1' }
      ],
      destructiveText: 'Delete',
      cancelText: 'Cancel',
      buttonClicked: function(i) {
        console.log(i, task_key);
        if (i == 0) { // -1
          var count = $scope.activeProject.chores[task_key].counts[$scope.currentUser];
          if (count > 0) count -= 1;
          $scope.activeProject.chores[task_key].counts[$scope.currentUser] = count;
        };
        return true;
      },
      destructiveButtonClicked: function() {
        console.log(task_key);
        delete $scope.activeProject.chores[task_key]
        return true;
      }
    })
   };

  // Try to create the first project, make sure to defer
  // this by using $timeout so everything is initialized
  // properly
  //$timeout(function() {
    // if($scope.projects.length == 0) {
    //   while(true) {
    //     var projectTitle = prompt('Your first project title:');
    //     if(projectTitle) {
    //       createProject(projectTitle);
    //       break;
    //     }
    //   }
    // }
  //}, 3000);
})

.filter('searchFor', function(){
  return function(arr, searchString){
    if(!searchString) return;
    if(searchString.length < 3) return [];

    var result = [];
    searchString = searchString.toLowerCase();

    angular.forEach(arr, function(item){
      if(item.name.toLowerCase().indexOf(searchString) !== -1)
        result.push(item);
    });

    console.log(result);

    return result;
  };
})

.controller('LoginCtrl', function($scope, $timeout, $state, Auth, $ionicHistory) {
  var ref = new Firebase("https://biwdy.firebaseio.com");

  var clearCacheAndGoToMain = function() {
    $ionicHistory.clearCache().then( function(){
      $state.go('main');
    });
  };

  // get current user
  $timeout(function() {
    if (ref.getAuth() !== null) clearCacheAndGoToMain();
  }, 3000)

  // find a suitable name based on the meta info given by each provider
  function getName(authData) {
    switch(authData.provider) {
      case 'facebook':
        return authData.facebook.displayName;
      case 'google':
        return authData.google.displayName;
    }
  }

  function getProfileImage(authData) {
    switch(authData.provider) {
      case 'facebook':
        return authData.facebook.profileImageURL;
      case 'google':
        return authData.google.profileImageURL;
    }
  }

  Auth.$onAuth( function(authData) {
    console.log(authData);
    if (authData) {
      // check if user is in database
      ref.child("Users").child(authData.uid).once('value', function(snapshot) {
        if (snapshot.val() === null) {
          ref.child("Users").child(authData.uid).set({
            provider: authData.provider,
            name: getName(authData),
            image: getProfileImage(authData)
          });
        }
        clearCacheAndGoToMain();
      });
    }
  });

  $scope.loginFb = function() {
    Auth.$authWithOAuthPopup("facebook").catch(function(error) {
      if (error.code === "TRANSPORT_UNAVAILABLE") {
        Auth.$authWithOAuthPopup("facebook").then(function(authData) {
          // User successfully logged in. We can log to the console
          // since we’re using a popup here
          console.log(authData);
        });
      } else {
        // Another error occurred
        console.log(error);
      }
    });
  }

  $scope.loginGoogle = function() {
    Auth.$authWithOAuthPopup("google").catch(function(error) {
      if (error.code === "TRANSPORT_UNAVAILABLE") {
        Auth.$authWithOAuthPopup("google").then(function(authData) {
          // User successfully logged in. We can log to the console
          // since we’re using a popup here
          console.log(authData);
        });
      } else {
        // Another error occurred
        console.log(error);
      }
    });
  }

  $scope.login = function(data) {
    ref.authWithPassword({
      email    : data.email,
      password : data.password
    }, function(error, authData) {
      console.log(error);
    }, {
      remember: "sessionOnly"
    });
  }

  $scope.signUp = function() {
    $state.go("signup");
  }
})

.factory("Auth", function($firebaseAuth) {
  var usersRef = new Firebase("https//biwdy.firebaseio.com/users");
  return $firebaseAuth(usersRef);
})

.controller('SignUpCtrl', function($scope, $timeout, $state, Auth) {
  var ref = new Firebase("https//biwdy.firebaseio.com");

  Auth.$onAuth( function(authData) {
    console.log(authData);
    if (authData) {
      $state.go('main');
    }
  });

  $scope.signUp = function(data) {
    ref.createUser(
      {
        email    : data.email,
        password : data.password
      },
      function(error, authData) {
        if (error) {
          console.log("Error creating user:", error);
        } else {
          console.log("Successfully created user account with uid:", authData.uid, data);
          console.log(authData);
          // Put user info in db
          ref.child("Users").child(authData.uid).set({
            provider: "password",
            name: data.name,
            image: "http://lorempixel.com/64/64/cats/" + Math.floor(Math.random()*9+1)
          });

          ref.authWithPassword({
            email    : data.email,
            password : data.password
          }, function(error, authData) {
            console.log(error);
          }, {
            remember: "sessionOnly"
          });
        }
      }
    );
  }
});
